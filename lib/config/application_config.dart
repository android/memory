import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:memory/cubit/activity/activity_cubit.dart';

import 'package:memory/ui/pages/game.dart';

class ApplicationConfig {
  // activity parameter: skin values
  static const String parameterCodeSkin = 'global.skin';
  static const String skinValueDefault = 'default';
  static const String skinValueAventure = 'aventure';
  static const String skinValueNature = 'nature';
  static const String skinValueItems = 'items';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Memory',
    activitySettings: [
      // skin
      ApplicationSettingsParameter(
        code: parameterCodeSkin,
        values: [
          ApplicationSettingsParameterItemValue(
            value: skinValueDefault,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: skinValueAventure,
          ),
          ApplicationSettingsParameterItemValue(
            value: skinValueNature,
          ),
          ApplicationSettingsParameterItemValue(
            value: skinValueItems,
          ),
        ],
        itemsPerLine: 2,
        builder: ({
          required context,
          required itemValue,
          required onPressed,
          required size,
        }) =>
            StyledButton(
          color: Colors.green.shade800,
          onPressed: onPressed,
          child: Table(
            children: [
              TableRow(children: [
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_1.png'),
                  fit: BoxFit.fill,
                ),
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_2.png'),
                  fit: BoxFit.fill,
                ),
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_3.png'),
                  fit: BoxFit.fill,
                ),
              ]),
              TableRow(children: [
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_4.png'),
                  fit: BoxFit.fill,
                ),
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_found.png'),
                  fit: BoxFit.fill,
                ),
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_5.png'),
                  fit: BoxFit.fill,
                ),
              ]),
              TableRow(children: [
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_6.png'),
                  fit: BoxFit.fill,
                ),
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_7.png'),
                  fit: BoxFit.fill,
                ),
                Image(
                  image: AssetImage('assets/skins/${itemValue.value}_8.png'),
                  fit: BoxFit.fill,
                ),
              ]),
            ],
          ),
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
