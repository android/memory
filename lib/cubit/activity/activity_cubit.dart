import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:memory/models/activity/activity.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createNull(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      shufflingInProgress: state.currentActivity.shufflingInProgress,
      // Base data
      board: state.currentActivity.board,
      // Game data
      guessesCount: state.currentActivity.guessesCount,
      pairsFound: state.currentActivity.pairsFound,
    );
    activity.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    refresh();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void allowInteractions(bool active) {
    state.currentActivity.animationInProgress = !active;
    refresh();
  }

  void unselectAllTiles() {
    for (int i = 0; i < state.currentActivity.board.tiles.length; i++) {
      state.currentActivity.board.tiles[i].selected = false;
    }
    refresh();
  }

  void markTilesAsPaired(List<int> tilesIndexes) {
    for (int i = 0; i < tilesIndexes.length; i++) {
      state.currentActivity.board.tiles[tilesIndexes[i]].paired = true;
    }
    refresh();
  }

  void tapOnTile(int tileIndex) {
    // Already selected? -> skip
    if (state.currentActivity.board.tiles[tileIndex].selected) {
      return;
    }

    // Already paired? -> skip
    if (state.currentActivity.board.tiles[tileIndex].paired) {
      return;
    }

    // Flip selected tile
    state.currentActivity.board.tiles[tileIndex].selected = true;
    refresh();

    // Is first tile selected?
    List<int> selectedTilesIndexes = [];
    for (int i = 0; i < state.currentActivity.board.tiles.length; i++) {
      if (state.currentActivity.board.tiles[i].selected) {
        selectedTilesIndexes.add(i);
      }
    }

    if (selectedTilesIndexes.length >= 2) {
      allowInteractions(false);

      // does all selected tiles have same value?
      bool hasSameValue = true;
      for (int i = 1; i < selectedTilesIndexes.length; i++) {
        if (state.currentActivity.board.tiles[selectedTilesIndexes[i]].value !=
            state.currentActivity.board.tiles[selectedTilesIndexes[i - 1]].value) {
          hasSameValue = false;
        }
      }

      state.currentActivity.guessesCount++;
      refresh();

      // timer + check pair + unselect
      Timer(const Duration(seconds: 2), () {
        if (hasSameValue) {
          markTilesAsPaired(selectedTilesIndexes);

          final int itemValue =
              state.currentActivity.board.tiles[selectedTilesIndexes[0]].value;
          state.currentActivity.pairsFound.add(itemValue);
          refresh();
        }

        state.currentActivity.isFinished = state.currentActivity.gameWon;
        refresh();

        unselectAllTiles();
        allowInteractions(true);
      });
    }
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
