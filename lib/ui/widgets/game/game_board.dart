import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:memory/cubit/activity/activity_cubit.dart';
import 'package:memory/models/activity/activity.dart';
import 'package:memory/ui/widgets/game/game_tile.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          final Color borderColor = Theme.of(context).colorScheme.onSurface;

          final int rowsCount = sqrt(currentActivity.board.tiles.length).toInt();
          final int colsCount = currentActivity.board.tiles.length ~/ rowsCount;

          return Container(
            margin: const EdgeInsets.all(2),
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              color: borderColor,
              borderRadius: BorderRadius.circular(2),
              border: Border.all(
                color: borderColor,
                width: 2,
              ),
            ),
            child: Table(
              children: [
                for (int row = 0; row < rowsCount; row++)
                  TableRow(
                    children: [
                      for (int col = 0; col < colsCount; col++)
                        GameTileWidget(
                          tileIndex: col + colsCount * row,
                        ),
                    ],
                  ),
              ],
            ),
          );
        },
      ),
    );
  }
}
