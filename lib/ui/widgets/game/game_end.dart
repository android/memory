import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:memory/config/application_config.dart';

import 'package:memory/cubit/activity/activity_cubit.dart';
import 'package:memory/models/activity/activity.dart';

class GameEndWidget extends StatelessWidget {
  const GameEndWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final Image decorationImage = Image(
          image: AssetImage(
              currentActivity.gameWon ? 'assets/ui/game_win.png' : 'assets/ui/game_fail.png'),
          fit: BoxFit.fill,
        );

        final double width = MediaQuery.of(context).size.width;

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          child: Table(
            defaultColumnWidth: FixedColumnWidth(width / 3.1),
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [decorationImage],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      currentActivity.animationInProgress == true
                          ? decorationImage
                          : ActivityButtonQuit(
                              onPressed: () {
                                BlocProvider.of<ActivityCubit>(context).quitActivity();
                                BlocProvider.of<NavCubitPage>(context)
                                    .updateIndex(ApplicationConfig.activityPageIndexHome);
                              },
                              color: Colors.blue,
                            ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [decorationImage],
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
