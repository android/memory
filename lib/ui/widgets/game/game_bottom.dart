import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:memory/config/application_config.dart';

import 'package:memory/cubit/activity/activity_cubit.dart';
import 'package:memory/models/activity/activity.dart';

class GameBottomWidget extends StatelessWidget {
  const GameBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        const Color borderColor = Colors.grey;
        final int values = currentActivity.board.tiles.length ~/ 2;

        List<Widget> items = [];
        for (int value = 1; value <= values; value++) {
          final String assetName =
              'assets/skins/${currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin)}_${currentActivity.pairsFound.contains(value) ? value.toString() : 'unknown'}.png';
          items.add(Padding(
            padding: const EdgeInsets.all(2),
            child: Image(
              image: AssetImage(assetName),
            ),
          ));
        }

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          decoration: BoxDecoration(
            color: borderColor,
            borderRadius: BorderRadius.circular(2),
            border: Border.all(
              color: borderColor,
              width: 2,
            ),
          ),
          child: Table(
            children: [
              TableRow(
                children: items,
              ),
            ],
          ),
        );
      },
    );
  }
}
