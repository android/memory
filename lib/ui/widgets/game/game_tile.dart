import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:memory/config/application_config.dart';

import 'package:memory/cubit/activity/activity_cubit.dart';
import 'package:memory/models/activity/activity.dart';
import 'package:memory/models/activity/tile.dart';

class GameTileWidget extends StatelessWidget {
  const GameTileWidget({
    super.key,
    required this.tileIndex,
  });

  final int tileIndex;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;
        final Tile tile = currentActivity.board.tiles[tileIndex];

        final String assetName =
            'assets/skins/${currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin)}_${tile.paired ? 'found' : (tile.selected ? tile.value.toString() : 'unknown')}.png';

        return Padding(
          padding: const EdgeInsets.all(2),
          child: AspectRatio(
            aspectRatio: 1,
            child: ElevatedButton(
              style: const ButtonStyle(
                padding: WidgetStatePropertyAll(EdgeInsets.all(2)),
              ),
              onPressed: () {
                if (!currentActivity.animationInProgress) {
                  BlocProvider.of<ActivityCubit>(context).tapOnTile(tileIndex);
                }
              },
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 100),
                transitionBuilder: (Widget child, Animation<double> animation) {
                  return ScaleTransition(
                    scale: animation,
                    child: child,
                  );
                },
                child: Image(
                  image: AssetImage(assetName),
                  fit: BoxFit.fill,
                  key: ValueKey<int>(assetName.hashCode),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
