import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:memory/config/application_config.dart';

import 'package:memory/models/activity/board.dart';

typedef MovingTile = String;
typedef Move = Board;
typedef Player = String;
typedef ConflictsCount = List<List<int>>;
typedef AnimatedBoard = List<List<bool>>;
typedef AnimatedBoardSequence = List<AnimatedBoard>;
typedef Word = String;

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,
    this.shufflingInProgress = false,

    // Base data
    required this.board,

    // Game data
    required this.guessesCount,
    required this.pairsFound,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;
  bool shufflingInProgress;

  // Base data
  final Board board;

  // Game data
  int guessesCount;
  List<int> pairsFound = [];

  factory Activity.createNull() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: Board.createNull(),
      // Game data
      guessesCount: 0,
      pairsFound: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final Board board = Board.createNew(
      activitySettings: newActivitySettings,
    );

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      board: board,
      // Game data
      guessesCount: 0,
      pairsFound: [],
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  bool get gameWon {
    return pairsFound.length == board.tiles.length ~/ 2;
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('    shufflingInProgress: $shufflingInProgress');
    printlog('  Base data');
    board.dump();
    printlog('  Game data');
    printlog('    guessesCount: $guessesCount');
    printlog('    pairsFound: $pairsFound');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      'shufflingInProgress': shufflingInProgress,
      // Base data
      'board': board.toJson(),
      // Game data
      'guessesCount': guessesCount,
      'pairsFound': pairsFound,
    };
  }
}
