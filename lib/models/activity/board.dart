import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:memory/models/activity/tile.dart';

class Board {
  Board({
    required this.tiles,
  });

  List<Tile> tiles = const [];

  factory Board.createNull() {
    return Board(
      tiles: [],
    );
  }
  factory Board.createNew({
    ActivitySettings? activitySettings,
  }) {
    List<Tile> tiles = [];

    const int itemsCount = 8;

    for (int i = 1; i <= itemsCount; i++) {
      tiles.add(Tile(
        value: i,
        paired: false,
        selected: false,
      ));
      tiles.add(Tile(
        value: i,
        paired: false,
        selected: false,
      ));
    }

    tiles.shuffle();

    return Board(
      tiles: tiles,
    );
  }

  void dump() {
    printlog('');
    printlog('$Board:');
    dumpGrid();
    printlog('');
  }

  void dumpGrid() {
    final int rowsCount = sqrt(tiles.length).toInt();
    final int colsCount = tiles.length ~/ rowsCount;

    for (int row = 0; row < rowsCount; row++) {
      String line = '  ';
      for (int col = 0; col < colsCount; col++) {
        final int tileIndex = col + row * colsCount;
        final Tile tile = tiles[tileIndex];
        line += '[${tile.value}${tile.selected ? 'S' : '.'}${tile.paired ? 'P' : '.'}]';
      }
      printlog(line);
    }
  }

  @override
  String toString() {
    return '$Board(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'tiles': tiles,
    };
  }
}
