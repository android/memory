import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

class Tile {
  Tile({
    required this.value,
    required this.selected,
    required this.paired,
  });

  int value = 0;
  bool selected = false;
  bool paired = false;

  factory Tile.createNull() {
    return Tile(
      value: 0,
      selected: false,
      paired: false,
    );
  }

  void dump() {
    printlog('');
    printlog('$Tile:');
    printlog('  value: $value');
    printlog('  selected: $selected');
    printlog('  paired: $paired');
    printlog('');
  }

  @override
  String toString() {
    return '$Tile(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'value': value,
      'selected': selected,
      'paired': paired,
    };
  }
}
